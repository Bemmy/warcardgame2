/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public class Card {
    
    private CardSuit suit;
    private CardValue value;
    
    public Card(CardSuit suit, CardValue value){
        this.suit = suit;
        this.value = value;
    }
    
    public CardSuit getSuit(){
        return suit;
    }
    
    public String toString(){
        return  value.name() + " of " + suit.name();
    }
    
}
