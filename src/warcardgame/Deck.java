/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

import java.util.List;
import java.util.ArrayList;

public class Deck {
    
    private List<Card> cards;
    
    public Deck(){
    
        cards = new ArrayList<Card>();
        
        for(CardSuit s : CardSuit.values()){
            for(CardValue v : CardValue.values()){
                Card card = new Card(s, v);
                cards.add(card);
                
                System.out.println(card);
            }
        }
       
    }
    
    public List<Card> getCard(){
        return cards;
    }
    
    public void setCards(List<Card> cards){
        this.cards = cards;
    }
     
}
